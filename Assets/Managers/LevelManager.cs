﻿using com.spacepuppy.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


[System.Serializable]
public class ColorToPrefabDictionary : SerializableDictionaryBase<Color32, GameObject>
{

}


public class LevelManager {
    private static LevelManager instance;
    private Texture2D levelMap;
    private ColorToPrefabDictionary colorLookup = new ColorToPrefabDictionary();
    private Dictionary<string, GameObject> prefabCache = new Dictionary<string, GameObject>();
    private World world;
    private LevelData levelData;

    public static LevelManager Instance {
        get {
            if(instance == null)
            {
                instance = new LevelManager();
            }

            return instance;
        }
    }

    public LevelManager() {
        this.notFoundPrefab = (GameObject)Resources.Load("Prefabs/Tiles/NotFoundPrefab");
    }

    public GameObject notFoundPrefab;
    public float WorldHeight
    {
        get
        {
            return levelMap.height;
        }
        private set { }
    }

    public float WorldWidth
    {
        get
        {
            return levelMap.width;
        }
        private set { }
    }
    
    public void SetLevelData(LevelData level)
    {
        this.levelData = level;
    }

    public void SetWorldObject(World world)
    {
        this.world = world;
    }

    public void PopulateWorld(World world)
    {
        this.world = world;
        PopulatePrefabCache(this.levelData.ColorLookup);
        byte[] imageBytes = System.IO.File.ReadAllBytes(Application.streamingAssetsPath + "/Level Maps/" + this.levelData.ImageMapPath);
        levelMap = new Texture2D(2, 2);
        levelMap.LoadImage(imageBytes);

        Color32[] pixels = levelMap.GetPixels32();
        int width = levelMap.width;
        int height = levelMap.height;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color32 c = pixels[y * width + x];

                if (c.a <= 0)
                {
                    continue;
                }

                InstantiatePrefab(c, x, y);

            }
        }
    }


    private void PopulatePrefabCache(ColorToPrefabPath lookup)
    {
        colorLookup.Clear();
        foreach (Color32 c32 in lookup.Keys)
        {
            string prefabPath = lookup[c32];
            if(prefabCache.ContainsKey(prefabPath))
            {
                continue;
            }
            else
            {
                GameObject newPrefab = (GameObject)Resources.Load(prefabPath);
                colorLookup.Add(c32, newPrefab);
            }
        }
    }

    private void InstantiatePrefab(Color32 color, int x, int y)
    {
        GameObject prefab;
        if (colorLookup.ContainsKey(color)) {
            prefab = colorLookup[color];
        }
        else {
            prefab = notFoundPrefab;
            Debug.Log("No prefab found for color: " + color.ToString());
        }
        //if(prefab == null) {
        //    return;
        //}

        this.world.SpawnTileAt(x, y, prefab);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.spacepuppy;
using com.spacepuppy.Collections;

[System.Serializable]
public class ColorToPrefabPath : SerializableDictionaryBase<Color32, string>
{

}

[System.Serializable]
public class LevelData {
    [SerializeField]
    public string LevelName;
    [SerializeField]
    public string ImageMapPath;
    [SerializeField]
    public ColorToPrefabPath ColorLookup;

}

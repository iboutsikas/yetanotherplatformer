﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushesFollowPlayer : MonoBehaviour {
    private GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindObjectOfType<PlayerController2D>().gameObject;

    }

    // Update is called once per frame
    void Update () {
        var currentPosition = this.transform.position;

        float targetX = player.transform.position.x;
        float targetY = this.transform.position.y;

        currentPosition.x = targetX;
        currentPosition.y = 2.5f;

        this.transform.position = currentPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster {
    private static GameMaster _instance { get; set; }

    public static GameMaster Instance
    {
        get {
            if (GameMaster._instance == null) GameMaster._instance = new GameMaster();

            return GameMaster._instance;
        }
    }
    
    public int Score { get; private set; }
    public bool Playing { get; set; }
    public bool GameOver { get; set; }

    public bool Paused { get; private set; }

    public bool Won { get; private set; }

    public int Lives { get; private set; }

    public GameMaster() {
        Score = 0;
        Lives = 3;
        Playing = false;
        GameOver = false;
    }

    public void IncreaseScore(int amount) {
        this.Score += amount;
    }

    public void PauseGame() {
        this.Playing = false;
        this.Paused = true;
    }

    public void WinGame() {
        this.Playing = false;
        this.Won = true;
    }

    public void ResetLives() {
        this.Lives = 3;
    }

    public void RemoveLife() {
        this.Lives--;
        if(Lives <= 0) {
            this.Playing = false;
            this.GameOver = true;
        }
    }

}

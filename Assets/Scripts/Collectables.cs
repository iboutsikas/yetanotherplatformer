﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectables : MonoBehaviour {


    void OnTriggerEnter2D(Collider2D col)
    {   
        Debug.Log("1");
        col.gameObject.SetActive(false);
        Destroy(col.gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("2");
        Destroy(col.gameObject);
    }
}

﻿using System;

public class ListItemClickedEventArgs: EventArgs
{
    public LevelData Level { get; private set; }

    public ListItemClickedEventArgs(LevelData levelData)
    {
        this.Level = levelData;
    }
}

public class ListItemSelectedEventArgs: EventArgs
{
    public LevelData SelectedLevel { get; private set; }

    public ListItemSelectedEventArgs(LevelData levelData)
    {
        this.SelectedLevel = levelData;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomLevelItem : MonoBehaviour {

    public LevelData Level;
    public Text text;
    public event EventHandler<ListItemClickedEventArgs> ItemClicked;


    // Use this for initialization
    void Start () {
        if (this.Level != null)
            this.Init(this.Level);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(LevelData newLevel)
    {
        this.Level = newLevel;
        if (text != null)
            this.text.text = this.Level.LevelName;
    }

    public void Self_OnClicked() {
        if(this.ItemClicked != null)
        {
            var args = new ListItemClickedEventArgs(this.Level);
            this.ItemClicked.Invoke(this, args);
        }
    }
}

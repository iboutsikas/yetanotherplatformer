﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CustomLevelList : MonoBehaviour {

    public Transform targetTransform;
    public CustomLevelItem itemPrefab;
    public GameObject playButton;
    public event EventHandler<ListItemSelectedEventArgs> ListItemSelected;
    public event EventHandler PlayButtonClicked;

    private void Start()
    {
        this.playButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
    }

    public void Init(List<LevelData> levels)
    {
        foreach (var level in levels)
        {
            CustomLevelItem display = Instantiate(itemPrefab);
            display.transform.SetParent(targetTransform, false);
            display.Init(level);
            display.ItemClicked += Display_ItemClicked;
        }
    }

    public void Display_ItemClicked(object sender, ListItemClickedEventArgs e)
    {
        if (this.ListItemSelected != null)
        {
            this.playButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
            this.ListItemSelected.Invoke(this, new ListItemSelectedEventArgs(e.Level));
        }
    }

    public void PlayButton_Clicked()
    {
        if (this.PlayButtonClicked != null)
            PlayButtonClicked.Invoke(this, new EventArgs());
    }
}

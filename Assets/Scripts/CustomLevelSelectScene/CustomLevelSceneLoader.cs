﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomLevelSceneLoader : MonoBehaviour {

    public CustomLevelList listPrefab;
    private LevelData selectedLevel;

	// Use this for initialization
	void Start () {
        DirectoryInfo dir = new DirectoryInfo(Application.streamingAssetsPath + "/Levels");
        FileInfo[] info = dir.GetFiles("*.json");
        CustomLevelList display = Instantiate(listPrefab);
        List<LevelData> levels = new List<LevelData>();

        foreach(var fi in info)
        {
            string jsonString = System.IO.File.ReadAllText(fi.FullName);
            LevelData data = (LevelData)JsonUtility.FromJson(jsonString, typeof(LevelData));
            levels.Add(data);
        }

        display.Init(levels);
        display.ListItemSelected += Display_ListItemSelected;
        display.PlayButtonClicked += Display_PlayButtonClicked;
    }

    private void Display_PlayButtonClicked(object sender, System.EventArgs e)
    {
        LevelManager.Instance.SetLevelData(this.selectedLevel);
        SceneManager.LoadScene(2);
    }

    private void Display_ListItemSelected(object sender, ListItemSelectedEventArgs e)
    {
        this.selectedLevel = e.SelectedLevel;
    }

    // Update is called once per frame
    void Update () {
		
	}
}

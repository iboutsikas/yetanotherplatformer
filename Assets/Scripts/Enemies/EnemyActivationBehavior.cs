﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActivationBehavior: MonoBehaviour {

    private MonoBehaviour[] scripts;

    private void Awake()
    {
        this.scripts = GetComponentsInChildren<MonoBehaviour>();
    }

    // Use this for initialization
    void Start () {
        SetProcessing(false);
	}

    private void OnBecameVisible()
    {
        SetProcessing(true);
    }

    private void OnBecameInvisible()
    {
        //SetProcessing(false);
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void SetProcessing(bool enable)
    {
        foreach (MonoBehaviour script in scripts)
        {
            script.enabled = enable;
        }
    }

    public void Die() {
        GameMaster.Instance.IncreaseScore(2);
        Destroy(gameObject);
    }
}

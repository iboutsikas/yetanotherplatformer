﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class EnemyMovementAI : MonoBehaviour {

    public GameObject target;

    public float updateRate = 2f;

    private Seeker seeker;
    private Rigidbody2D rb;

    public Path path;

    public float speed = 300f;
    public ForceMode2D fMode;

    [HideInInspector]
    public bool pathIsEnded = false;

    public float nextWaypointDistance = 3;

    private int currentWaypoint = 0;


	// Use this for initialization
	void Start () {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if(target == null)
        {
            target = GameObject.FindObjectOfType<PlayerController2D>().gameObject;
        }
        var tarPos = target.transform.position;
        seeker.StartPath(this.transform.position, tarPos, OnPathComplete);

        StartCoroutine(UpdatePath());
	}
	
	// Update is called once per frame
	void Update () {
	}

    void FixedUpdate()
    {
        if(target == null)
        {
            return;
        }

        if(path == null) return;

        if(currentWaypoint >= path.vectorPath.Count)
        {
            if (pathIsEnded) return;
            pathIsEnded = true;
            return;
        }

        pathIsEnded = false;
        //Direction to next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        
        dir *= speed * Time.fixedDeltaTime;

        //Move the AI towards the player
        rb.AddForce(dir, fMode);
        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);

        if (dist < nextWaypointDistance)
        {
            if (dir.x < 0) {
                transform.localScale = new Vector3(-1, 1, 1);
            } else {
                transform.localScale = new Vector3(1, 1, 1);
            }
            currentWaypoint++;
            return;
        }

    }

    public void OnPathComplete(Path p)
    {
        if(!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    IEnumerator UpdatePath()
    {
        if(target == null)
        {
            // TODO: perhaps search for player here?
            yield return null;
        }

        seeker.StartPath(transform.position, target.transform.position, OnPathComplete);
        yield return new WaitForSeconds(1f / updateRate);
        StartCoroutine(UpdatePath());
    }
}

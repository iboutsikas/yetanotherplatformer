﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class World : MonoBehaviour {
    private LevelManager levelManager;
    private Text scoreTextObject;
    private Text gameStateObject;
    private GameMaster gm;
    private GameObject[] Hearts;

    public GameObject HealthUI;
    public GameObject HeartPrefab;
    public GameObject Menu;

    // Use this for initialization
    void Start () {
        Debug.Log("World on Start");
        this.levelManager = LevelManager.Instance;
        this.gm = GameMaster.Instance;
        this.gm.ResetLives();
        this.scoreTextObject = GameObject.FindGameObjectWithTag("Score").GetComponent<Text>();
        this.gameStateObject = GameObject.FindGameObjectWithTag("Game State").GetComponent<Text>();
        gameStateObject.enabled = false;
        Menu.SetActive(false);
        Time.timeScale = 1;
        gm.Playing = true;
        levelManager.PopulateWorld(this);
        AstarPath.active.logPathResults = PathLog.None;
        CalculatePathfindingGrid();

        Hearts = new GameObject[gm.Lives];

        var uiPosition = HealthUI.transform.position;
        for (int i = 0; i < gm.Lives; i++) {
            var heartGO = Instantiate(HeartPrefab, HealthUI.transform, false);
            heartGO.GetComponent<RectTransform>().position = new Vector3(5 + i * 30, uiPosition.y - 20, uiPosition.z);
            Hearts[i] = heartGO;
        }
    }
	
	// Update is called once per frame
	void Update () {
        this.scoreTextObject.text = string.Format("Score: {0}", this.gm.Score);
        
        for(int i = 0; i < Hearts.Length; i++) {
            if(i >= gm.Lives) {
                Hearts[i].GetComponent<Image>().enabled = false;
            }
        }

        if(Input.GetButtonUp("Pause")) {
            gm.PauseGame();
        }

        if(!gm.Playing && gm.Paused) {
            Time.timeScale = 0;
            gameStateObject.enabled = true;
            gameStateObject.text = "Paused";
            Menu.SetActive(true);
        }

        if (!gm.Playing && gm.Won) {
            Time.timeScale = 0;
            gameStateObject.enabled = true;
            gameStateObject.text = "Congratulations";
            Menu.SetActive(true);
        }

        if (!gm.Playing && gm.GameOver) {
            Time.timeScale = 0;
            gameStateObject.enabled = true;
            gameStateObject.text = "Game Over";
            Menu.SetActive(true);
        }
    }

    public void SpawnTileAt(int x, int y, GameObject prefab)
    {
        GameObject go = Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity, this.transform);
    }

    private void CalculatePathfindingGrid()
    {
        AstarData data = AstarPath.active.data;

        GridGraph gg = data.AddGraph(typeof(GridGraph)) as GridGraph;

        gg.center = new Vector3((int)levelManager.WorldWidth / 2, (int)levelManager.WorldHeight /2 , -0.1f);
        gg.SetDimensions((int)levelManager.WorldWidth, (int)levelManager.WorldHeight, 1f);
        gg.rotation = new Vector3(-90, 0, 0);

        //var mask = LayerMask.GetMask("Static Enviroment", "Enemy Invisible Walls");
        var mask = 1 << 8 | 1 << 10;
        //gg.drawGizmos = true;
        //gg.showMeshSurface = true;
        //gg.showMeshOutline = true;
        //gg.showNodeConnections = true;
        gg.collision.mask = mask;
        gg.collision.use2D = true;
        gg.collision.type = ColliderType.Sphere;
        gg.erodeIterations = 0;
        //gg.useRaycastNormal = true;
      

        AstarPath.active.Scan();

        //foreach(var obs in obstacles)
        //{
        //    var collider = obs.GetComponent<Collider2D>();
        //    if (collider != null)
        //    {
        //        var bounds = collider.bounds;
        //        bounds.Expand(Vector3.forward * 1000);
        //        AstarPath.active.UpdateGraphs(bounds);
        //    }
        //}
    }

    public void OnBackButton_Clicked() {
        SceneManager.LoadScene(0);
    }
}

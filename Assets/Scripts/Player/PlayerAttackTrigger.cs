﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackTrigger : MonoBehaviour {
    private void OnCollisionEnter2D(Collision2D collision) {
        var enemy = collision.gameObject.GetComponent<EnemyActivationBehavior>();
        if (enemy != null) {
            enemy.Die();
        }
    }
}

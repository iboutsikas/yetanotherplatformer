﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillFieldFollowPlayer : MonoBehaviour {

    GameObject player;

    // Use this for initialization
    void Start () {
        player = GameObject.FindObjectOfType<PlayerController2D>().gameObject;
    }
	
	// Update is called once per frame
	void Update () {

        //if (player == null) player = GameObject.FindObjectOfType<PlayerController2D>().gameObject;


        var currentPosition = this.transform.position;

        float targetX = player.transform.position.x;
        float targetY = this.transform.position.y;

        currentPosition.x = targetX;
        currentPosition.y = targetY;

        this.transform.position = currentPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision) {

        Debug.Log("We collided");
        if (collision.gameObject.CompareTag("Player")) {
            Debug.Log("It was A Player");
            player.GetComponent<PlayerController2D>().enabled = false;
            GameMaster.Instance.Playing = false;
            GameMaster.Instance.GameOver = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {
    GameObject player;
    LevelManager levelManager;
    public float minX = 5.10f;
    public float minY = 4.3f;
    public float maxX;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerController2D>().gameObject;
        maxX = LevelManager.Instance.WorldWidth - minX;
    }

    // Update is called once per frame
    void Update()
    {

        var currentPosition = this.transform.position;
        
        float targetX = Mathf.Clamp(player.transform.position.x, minX, maxX);
        float targetY = Mathf.Clamp(player.transform.position.y, minY, float.PositiveInfinity);
        //float targetY = player.transform.position.y;

        currentPosition.x = targetX;
        currentPosition.y = targetY;

        this.transform.position = currentPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {
    public int DistanceToMove;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        var cameraPos = Camera.main.transform.position;
        var selfPos = this.transform.position;
        var dist = cameraPos.x - selfPos.x;


        if (dist > DistanceToMove) {
            this.transform.position = new Vector3(this.transform.position.x + 50, 7, 0);
        } else if (dist < -DistanceToMove) {
            this.transform.position = new Vector3(this.transform.position.x - 50, 7, 0);
        }
    }
}
